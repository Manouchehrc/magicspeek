﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MagicSpeek.TelegramHelper
{
    public class TelegramTypes
    {
        #region Class Type

        public class X_MessageEntity
        {
            public string type { get; set; }
            public int offset { get; set; }
            public int length { get; set; }
        }

        public class X_PhotoSize
        {
            public string file_id { get; set; }
            public int width { get; set; }
            public int Height { get; set; }
        }

        public class X_QueryResultVenue
        {
            public readonly string Type = "Venue";
            public int id;
            public double latitude;
            public double longitude;
            public string title;
            public string address;
        }

        public class X_User
        {
            public Int64 id;
            public string first_name;
            public string last_name;
            public string username;
            public string language_code;
        }

        public class X_From
        {
            public int id { get; set; }
            public bool is_bot { get; set; }
            public string first_name { get; set; }
            public string username { get; set; }
        }

        public class X_Chat
        {
            public Int64 id;
            public string type;
            public string title;
            public string username;
            public string first_name;
            public string last_name;
            public bool all_members_are_administrators;
            public X_Photo photo;
            public string description;
            public string invite_link;
        }

        public class X_Message
        {
            public long message_id;
            public X_User from;
            public long date;
            public X_Chat chat;
            public string forward_from;                 //optional
            public string forward_from_chat;            //optional
            public string forward_from_message_id;      //optional
            public string forward_date;                 //optional
            public string reply_to_message;             //optional
            public string edit_date;                    //optional
            public string text;                         //optional
            public List<X_Entity> entities;             //optional
            public X_Audio audio;                       //optional
            public X_Document document;                 //optional
            public string game;                         //optional
            public List<X_PhotoSize> photo;             //optional
            public X_Sticker sticker;                   //optional
            public X_Video video;                       //optional
            public X_Voice voice;                       //optional
            public X_VideoNote video_note;              //optional
            public string new_chat_members;             //optional
            public string caption;                      //optional
            public X_Contact contact;                   //optional
            public X_Location location;                 //optional
            public X_Venue venue;                       //optional
            public string new_chat_member;              //optional
            public string left_chat_member;             //optional
            public string new_chat_title;               //optional
            public string new_chat_photo;               //optional
            public string delete_chat_photo;            //optional
            public string group_chat_created;           //optional
            public string supergroup_chat_created;      //optional
            public string channel_chat_created;         //optional
            public string migrate_to_chat_id;           //optional
            public string migrate_from_chat_id;         //optional
            public string pinned_message;               //optional
            public string invoice;                      //optional
            public string successful_payment;           //optional
            public string SuccessfulPayment;            //optional
        }

        public class X_Entity
        {
            public string type { get; set; }
            public int offset { get; set; }
            public int length { get; set; }
        }

        public class X_Voice
        {
            public int duration { get; set; }
            public string mime_type { get; set; }
            public string file_id { get; set; }
            public int file_size { get; set; }
        }

        public class X_Photo
        {
            public string file_id { get; set; }
            public int file_size { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class X_Contact
        {
            public string phone_number;
            public string first_name;
            public string last_name;	                 //Optional
            public Int64 user_id;                        //Optional
        }

        public class X_Location
        {
            private float longitude;
            public float latitude;
        }

        public class X_CallbackQuery
        {
            public string id;
            public X_User from;
            public X_Message message;
            public string inline_message_id;
            public string chat_instance;
            public string data;
            public string game_short_name;
        }

        public class X_Update
        {
            public long update_id;
            public X_Message message;
            public X_Message edited_message;
            public string channel_post;
            public string edited_channel_post;
            public X_InlineQuery inline_query;
            public string chosen_inline_result;
            public X_CallbackQuery callback_query;
            public string shipping_query;
            public string pre_checkout_query;
        }

        public class X_InlineQuery
        {
            public string id;
            public X_User from;
            public string Location;
            public string query;
            public string offset;
        }

        public class X_InlineKeyboardButton
        {
            public string text;
            public string url;
            public string callback_data;
            public string switch_inline_query;
            public string switch_inline_query_current_chat;
            public string callback_game;
        }

        public class X_InlineKeyboardMarkup
        {
            public List<List<X_InlineKeyboardButton>> inline_keyboard = new List<List<X_InlineKeyboardButton>>();

            private List<X_InlineKeyboardButton> _buttonList = new List<X_InlineKeyboardButton>();
            public void InsertButton(X_InlineKeyboardButton aButton)
            {
                _buttonList.Add(aButton);
            }

            public void EndRow()
            {
                inline_keyboard.Add(_buttonList);
                _buttonList = new List<X_InlineKeyboardButton>();
            }
        }

        public class X_KeyboardButton
        {
            public string text;
            public bool request_contact;
            public bool request_location;
        }

        public class X_ReplyKeyboardMarkup
        {
            public List<List<X_KeyboardButton>> keyboard = new List<List<X_KeyboardButton>>();
            public bool resize_keyboard;
            public bool one_time_keyboard;
            public bool selective;

            private List<X_KeyboardButton> _buttonList = new List<X_KeyboardButton>();
            public void InsertButton(X_KeyboardButton aButton)
            {
                _buttonList.Add(aButton);
            }

            public void EndRow()
            {
                keyboard.Add(_buttonList);
                _buttonList = new List<X_KeyboardButton>();
            }
        }

        public class X_JObjectResult
        {
            public string ok;
            public dynamic result;
            public String ERROR { get; set; }
        }

        public class X_ReplyKeyboardRemove
        {
            public bool remove_keyboard = true;
        }

        public class X_InlineQueryResultArticle
        {
            public readonly string type = "article";
            public string id;
            public string title;
            public X_InputTextMessageContent input_message_content;
            public X_InlineKeyboardMarkup reply_markup;
            public string url;
            public Boolean hide_url;
            public string description;
            public string thumb_url;
            public int thumb_width;
            public int thumb_height;
        }

        public class X_InputTextMessageContent
        {
            public string message_text;
            public string parse_mode;
            public Boolean disable_web_page_preview;
        }

        public class X_Venue
        {
            public X_Location location { get; set; }
            public string title { get; set; }
            public string address { get; set; }
        }

        public class X_Video
        {
            public int duration { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public string mime_type { get; set; }
            public X_Thumb thumb { get; set; }
            public string file_id { get; set; }
            public int file_size { get; set; }
        }

        public class X_Thumb
        {
            public string file_id { get; set; }
            public int file_size { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }
        #endregion

        #region Return Values in JSON
        public class X_Json_Update
        {
            public bool ok { get; set; }
            public List<X_Update> result { get; set; }
        }

        public class X_Json_EditMessageText
        {
            public bool ok { get; set; }
            public Result result { get; set; }

            public class Result
            {
                public int message_id { get; set; }
                public X_User from { get; set; }
                public X_Chat chat { get; set; }
                public int date { get; set; }
                public string text { get; set; }
            }

        }

        public class X_Json_File_Path
        {
            public string file_id { get; set; }
            public int file_size { get; set; }
            public string file_path { get; set; }
        }

        public class X_Json_GetUserProfilePhotos
        {
            public bool ok { get; set; }
            public Result result { get; set; }

            public class Result
            {
                public int total_count { get; set; }
                public List<List<String>> photos { get; set; }
            }

        }

        public class X_Json_GetMe
        {
            public bool ok { get; set; }
            public Result result { get; set; }

            public class Result
            {
                public int id { get; set; }
                public string first_name { get; set; }
                public string username { get; set; }
            }


        }

        public class X_Json_SendMessage
        {
            public int message_id { get; set; }
            public X_User from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public string text { get; set; }

        }

        public class X_Json_WebHook
        {
            public int update_id { get; set; }
            public X_Message message { get; set; }
        }

        public class X_Json_VoiceMessage
        {
            public int update_id { get; set; }
            public X_Message message { get; set; }
        }

        //After we send voice message by bot, we receive below json
        public class X_Json_AfterSendVoiceMessage
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public X_Voice voice { get; set; }
        }

        public class X_Json_AfterSendLocation
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public X_Location location { get; set; }
        }

        public class X_Json_AfterSendVenue
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public X_Location location { get; set; }
            public X_Venue venue { get; set; }
        }

        public class X_Json_AfterSendContact
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public X_Contact contact { get; set; }
        }

        public class X_Json_AfterSendPhoto
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public List<X_Photo> photo { get; set; }
        }

        public class X_Json_AfterSendVideo
        {
            public int message_id { get; set; }
            public X_From from { get; set; }
            public X_Chat chat { get; set; }
            public int date { get; set; }
            public X_Video video { get; set; }
        }
        #endregion

        #region Send Telegram Method Parameters

        public class Action_answerCallbackQuery
        {
            public string callback_query_id { get; set; }
            public string text	{ get; set; }
            public bool show_alert { get; set; }
            public string url { get; set; }
            public int cache_time { get; set; }
        }

        public class Action_SendVoice
        {
            public long chat_id { get; set; }
            public string voice { get; set; }
            public string caption { get; set; }
            public long? duration { get; set; }
            public bool? disable_notification { get; set; }
            public long? reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendPhoto
        {
            public long chat_id { get; set; }
            public string photo { get; set; }
            public string caption { get; set; }
            public bool? disable_notification { get; set; }
            public long? reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendDocument
        {
            public long chat_id { get; set; }
            public string document { get; set; }
            public string caption { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendVideo
        {
            public long chat_id { get; set; }
            public string video { get; set; }
            public long duration { get; set; }
            public long width { get; set; }
            public long height { get; set; }
            public string caption { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendLocation
        {
            public long chat_id { get; set; }
            public float latitude { get; set; }
            public float longitude { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendVenu
        {
            public long chat_id { get; set; }
            public float latitude { get; set; }
            public float longitude { get; set; }
            public string title { get; set; }
            public string address { get; set; }
            public string foursquare_id { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendContact
        {
            public long chat_id { get; set; }
            public string phone_number { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_editMessageText
        {
            public long chat_id { get; set; }
            public long message_id { get; set; }
            public string inline_message_id { get; set; }
            public string text { get; set; }
            public string parse_mode { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_editMessageCaption
        {
            public long chat_id { get; set; }
            public long message_id { get; set; }
            public string inline_message_id { get; set; }
            public string caption { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_editMessageReplyMarkup
        {
            public long chat_id { get; set; }
            public long message_id { get; set; }
            public string inline_message_id { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_SendMessage
        {
            public long chat_id { get; set; }
            public string text { get; set; }
            public string parse_mode { get; set; }
            public bool disable_notification { get; set; }
            public long reply_to_message_id { get; set; }
            public bool disable_web_page_preview { get; set; }
            public dynamic reply_markup { get; set; }
        }

        public class Action_getFile
        {
            public String file_id { get; set; }
        }

        public class Action_AnswerInlineQuery
        {
            public string inline_query_id { set; get; }
            public List<dynamic> results = new List<dynamic>();
            public int cache_time { set; get; }
            public int is_personal { set; get; }
            public string next_offset { set; get; }
            public string switch_pm_text { set; get; }
            public string switch_pm_parameter { set; get; }

            public void AddQueryResult(dynamic aQResult)
            {
                results.Add(aQResult);
            }
        }

        #endregion

        public class X_Sticker
        {
            public string file_id { set; get; }
            public int width { set; get; }
            public int height { set; get; }
            public X_PhotoSize thumb { set; get; }
            public string emoji { set; get; }
            public string set_name { set; get; }
            public string mask_position { set; get; }
            public int file_size { set; get; }
        }

        public class X_VideoNote
        {
            public string file_id { set; get; }
            public int length { set; get; }
            public int duration { set; get; }
            public X_PhotoSize thumb { set; get; }
            public int file_size { set; get; }
        }

        public class X_Document
        {
            public string file_id { set; get; }
            public X_PhotoSize thumb { set; get; }
            public string file_name	 { set; get; }
            public string mime_type	 { set; get; }
            public int file_size { set; get; }
        }

        public class X_Audio
        {
            public string file_id { set; get; }
            public int duration { set; get; }
            public string performer { set; get; }
            public string title { set; get; }
            public string mime_type	 { set; get; }
            public int file_size  { set; get; }
        }
    }
}