﻿using MagicSpeek.Models;
using MagicSpeek.TelegramHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace MagicSpeek.Areas.MagicSpeek.Controllers
{
    /// <summary>
    /// Webhook: https://api.telegram.org/bot388101817:AAHVLLBn-so1XDlGfCijzYsXIMgLF3JxG6Y/SetWebHook?url=https://pooyafaroka.localtunnel.me/MagicSpeek/Bot/
    /// Port: 2541
    /// ngrok: ngrok http 2541 -host-header="localhost:2541"
    /// NPM: iis-lt --port 2541 --subdomain pooyafaroka
    /// </summary>
    public class BotController : Controller, IMessageProcessor
    {
        public TelegramInterface _telegram_interface;
        
        public ActionResult Index()
        {
            _telegram_interface = new TelegramInterface(WebConfigurationManager.AppSettings["TELEGRAM_dor_o_bar_bot_API_KEY"]);
            _telegram_interface.ParseUpdate(Request.InputStream, this);

            return Content("{\"ok\":\"false\", \"result\":\"No data\"}", "application/json");
        }

        public String OnRecievedInlineQuery(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedCallbackQuery(TelegramTypes.X_Update xCallback)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            long chat_id = xCallback.callback_query.from.id;
            long message_id = xCallback.callback_query.message.message_id;
            String str_current_level = "";
            String str_current_sublevel = "";
            int i_current_level = 0;
            int i_current_sublevel = 0;
            int points = 0;

            if (xCallback.callback_query.data.Contains("LEVEL"))
            {
                switch (xCallback.callback_query.data)
                {
                    case "LEVEL.NEXT":
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        if (i_current_sublevel != 20)
                        {
                            i_current_sublevel++;
                        }
                        else
                        {
                            i_current_sublevel = 1;
                        }
                        points = DBHelper.getUserLevelPoints(chat_id, i_current_level, i_current_sublevel);
                        HandleLevelMenuByPoint(xCallback, i_current_level, i_current_sublevel, points);

                        break;

                    case "LEVEL.PREV":
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        if (i_current_sublevel != 1)
                        {
                            i_current_sublevel--;
                        }
                        else
                        {
                            i_current_sublevel = 20;
                        }
                        points = DBHelper.getUserLevelPoints(chat_id, i_current_level, i_current_sublevel);
                        HandleLevelMenuByPoint(xCallback, i_current_level, i_current_sublevel, points);

                        break;

                    case "LEVEL.SELECT":
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        points = DBHelper.getUserLevelPoints(chat_id, i_current_level, i_current_sublevel);
                        List<String> conversionLines = DBHelper.getConversation(i_current_level, i_current_sublevel);

                        String wholeConversion = "#Conversion #Level_" + i_current_level.ToString() + " #Sublevel_" + i_current_sublevel.ToString() + "\n";
                        for (int i = 0; i < conversionLines.Count; i++)
                        {
                            wholeConversion += "🔸" + conversionLines[i] + "\n\n";
                        }
                        inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                        inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                        {
                            text = "Dialog Voice",
                            callback_data = "WHOLE_VOICE"
                        });
                        inlineKeyboardMarkup.EndRow();
                        _telegram_interface.SendMessage(chat_id, wholeConversion, inlineKeyboardMarkup);
                        break;
                }
            }
            else if (xCallback.callback_query.data.Contains("WHOLE_VOICE"))
            {
                ParseHashTagedLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                i_current_level = Convert.ToInt16(str_current_level);
                i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                String conversionVoice = DBHelper.getConversionVoice(chat_id, i_current_level, i_current_sublevel);
                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "Next Level",
                    callback_data = "SINGLE_VOICE"
                });

                inlineKeyboardMarkup.EndRow();
                _telegram_interface.SendVoice(conversionVoice, chat_id, "#Voice " + "#Level_" + i_current_level + " #Sublevel_" + i_current_sublevel, inlineKeyboardMarkup);
            }
            else if (xCallback.callback_query.data.Contains("SINGLE_VOICE"))
            {
                switch (xCallback.callback_query.data)
                {
                    case "SINGLE_VOICE":
                        _telegram_interface.SendMessage(chat_id, "You can listen whole conversion part by part. Just click on NEXT or PREVIEW button. Let's start.");
                        inlineKeyboardMarkup = CreateVoiceMenu(true);
                        str_current_level = "";
                        str_current_sublevel = "";
                        ParseHashTagedLevel(xCallback.callback_query.message.caption, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        _telegram_interface.SendMessage(chat_id,
                            CreateVoiceUrl(str_current_level, str_current_sublevel, IntToString(DBHelper.getConversation(i_current_level, i_current_sublevel).Count), "01")
                            + "\n\n"
                            + DBHelper.getConversionLine(i_current_level, i_current_sublevel, Convert.ToInt16("01"))
                            , inlineKeyboardMarkup, false, false, 0, "HTML");
                        break;

                    case "SINGLE_VOICE.NEXT":
                        String str_current_line = "";
                        String str_total_count = "";
                        message_id = xCallback.callback_query.message.message_id;
                        ParseVoiceOneLineLable(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_total_count, out str_current_line);
                        int next_line = Convert.ToInt16(str_current_line);
                        String str_next_line = "";
                        if (next_line != Convert.ToInt16(str_total_count))
                        {
                            next_line++;
                        }
                        else
                        {
                            next_line = 1;
                        }
                        str_next_line = IntToString(next_line);
                        inlineKeyboardMarkup = CreateVoiceMenu(true);
                        _telegram_interface.editMessageText(chat_id, message_id,
                            CreateVoiceUrl(str_current_level, str_current_sublevel, str_total_count, str_next_line)
                            + "\n\n"
                            + DBHelper.getConversionLine(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_next_line))
                            , inlineKeyboardMarkup, "HTML");
                        break;

                    case "SINGLE_VOICE.PREV":
                        str_current_line = "";
                        str_total_count = "";
                        message_id = xCallback.callback_query.message.message_id;
                        ParseVoiceOneLineLable(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_total_count, out str_current_line);
                        next_line = Convert.ToInt16(str_current_line);
                        str_next_line = "";
                        if (next_line != 1)
                        {
                            next_line--;
                        }
                        else
                        {
                            next_line = Convert.ToInt16(str_total_count);
                        }
                        str_next_line = IntToString(next_line);
                        inlineKeyboardMarkup = CreateVoiceMenu(true);
                        _telegram_interface.editMessageText(chat_id, message_id,
                            CreateVoiceUrl(str_current_level, str_current_sublevel, str_total_count, str_next_line)
                            + "\n\n"
                            + DBHelper.getConversionLine(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_next_line))
                            , inlineKeyboardMarkup, "HTML");
                        break;

                    case "SINGLE_VOICE.FINISH":
                        str_current_line = "";
                        str_total_count = "";
                        message_id = xCallback.callback_query.message.message_id;
                        ParseVoiceOneLineLable(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_total_count, out str_current_line);
                        next_line = Convert.ToInt16(str_current_line);
                        str_next_line = IntToString(next_line);
                        inlineKeyboardMarkup = CreateQuizeMenu();
                        List<String> Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), 0);
                        _telegram_interface.SendMessage(chat_id, CreateQuize(Quize, str_current_level, str_current_sublevel), inlineKeyboardMarkup);
                        break;
                }
            }
            else if (xCallback.callback_query.data.Contains("QUIZE"))
            {
                String str_current_quize = "";
                String str_total_quize = "";
                int Answer = 0;
                int i_current_quize = 0;
                int i_total_quize = 0;
                ParseQuize(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_current_quize, out str_total_quize);
                i_current_quize = Convert.ToInt16(str_current_quize);
                i_total_quize = Convert.ToInt16(str_total_quize);
                switch (xCallback.callback_query.data)
                {
                    case "QUIZE.1":
                        Answer = 0;
                        break;

                    case "QUIZE.2":
                        Answer = 1;
                        break;

                    case "QUIZE.3":
                        Answer = 2;
                        break;

                    case "QUIZE.4":
                        Answer = 3;
                        break;
                }
                List<String> Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_current_quize) - 1);
                _telegram_interface.editMessageText(chat_id, message_id, CheckQuize(Quize, str_current_level, str_current_sublevel, Answer));

                if(i_current_quize == i_total_quize)
                {
                    _telegram_interface.SendMessage(chat_id, "💠💠💠💠💠💠💠💠💠💠💠");
                }
                else
                {
                    Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_current_quize));
                    inlineKeyboardMarkup = CreateQuizeMenu();
                    _telegram_interface.SendMessage(chat_id, CreateQuize(Quize, str_current_level, str_current_sublevel), inlineKeyboardMarkup);
                }
            }
            return null;
        }

        public String OnRecievedVoice(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedAudio(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVideo(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVideoNote(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedDocument(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedPhoto(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedContact(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedLocation(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVenue(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedMessage(TelegramTypes.X_Update xMessage)
        {
            long chat_id = xMessage.message.chat.id;
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = CreateLevelMenu(true);
            int level = 0;
            int sublevel = 0;
            int points = 0;
            bool isNewUser = false;

            DBHelper.getUserLevel(chat_id, 123, xMessage.message.from.first_name, xMessage.message.from.last_name, xMessage.message.from.username, out isNewUser, out level, out sublevel, out points);
            _telegram_interface.SendMessage(chat_id, CreateLevelImageUrl(chat_id, level, sublevel, points), inlineKeyboardMarkup, false, false, 0, "HTML");

            return null;
        }

        /// <summary>
        /// Create level menu with glassly button.
        /// </summary>
        /// <param name="xUpdate"></param>

        private TelegramTypes.X_InlineKeyboardMarkup CreateLevelMenu(bool hasSelectButton)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "PREVIEW",
                callback_data = "LEVEL.PREV"
            });

            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "NEXT",
                callback_data = "LEVEL.NEXT"
            });

            inlineKeyboardMarkup.EndRow();
            if(hasSelectButton)
            {
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "SELECT LEVEL",
                    callback_data = "LEVEL.SELECT"
                });
                inlineKeyboardMarkup.EndRow();
            }
            
            return inlineKeyboardMarkup;
        }

        /// <summary>
        /// Create a menu for listening void line by line.
        /// </summary>
        /// <param name="hasFinishButton"></param>
        /// <returns></returns>
        private TelegramTypes.X_InlineKeyboardMarkup CreateVoiceMenu(bool hasFinishButton)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "PREVIEW",
                callback_data = "SINGLE_VOICE.PREV"
            });

            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "NEXT",
                callback_data = "SINGLE_VOICE.NEXT"
            });

            inlineKeyboardMarkup.EndRow();
            if (hasFinishButton)
            {
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "FINISH",
                    callback_data = "SINGLE_VOICE.FINISH"
                });
                inlineKeyboardMarkup.EndRow();
            }

            return inlineKeyboardMarkup;
        }

        /// <summary>
        /// Create Quize Menu
        /// </summary>
        /// <returns></returns>
        private TelegramTypes.X_InlineKeyboardMarkup CreateQuizeMenu()
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "1",
                callback_data = "QUIZE.1"
            });
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "2",
                callback_data = "QUIZE.2"
            });
            inlineKeyboardMarkup.EndRow();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "3",
                callback_data = "QUIZE.3"
            });
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "4",
                callback_data = "QUIZE.4"
            });
            inlineKeyboardMarkup.EndRow();

            return inlineKeyboardMarkup;
        }

        private String CreateQuize(List<String> Quize, String current_level, String current_sublevel)
        {
            String retValue = "🔸 Level: " + current_level + "_" + current_sublevel + "," + " Quize: " + Quize[0] + "\n";
            retValue += "💠 " + Quize[1] + "\n"
                        + "\n1️⃣ " + Quize[2]
                        + "\n2️⃣ " + Quize[3]
                        + "\n3️⃣ " + Quize[4]
                        + "\n4️⃣ " + Quize[5];

            return retValue;
        }

        private String CheckQuize(List<String> Quize, String current_level, String current_sublevel, int Answer)
        {
            String[] numChar = { "1️⃣", "2️⃣", "3️⃣", "4️⃣" };
            String retValue = "🔸 Level: " + current_level + "_" + current_sublevel + "," + " Quize: " + Quize[0] + "\n";
            retValue += "💠 " + Quize[1] + "\n";
            int trueAnswer = 0;
            for (int i = 2; i < Quize.Count; i++)
            {
                if (Quize[i].Equals(Quize[6]))
                {
                    trueAnswer = i - 2;
                    break;
                }
            }
            if(Quize[6].Equals(Quize[Answer + 2]))
            {
                for (int i = 0; i < 4; i++)
                {
                    if (Answer == i)
                    {
                        retValue += "\n" + "✅" + " " + Quize[i + 2];
                    }
                    else
                    {
                        retValue += "\n" + numChar[i] + " " + Quize[i + 2];
                    }
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    if(Answer == i)
                    {
                        retValue += "\n" + "❌" + " " + Quize[i + 2];
                    }
                    else if(trueAnswer == i)
                    {
                        retValue += "\n" + "✅" + " " + Quize[i + 2];
                    }
                    else
                    {
                        retValue += "\n" + numChar[i] + " " + Quize[i + 2];
                    }
                }
            }

            return retValue;
        }

        private void ParseQuize(String text, out String current_level, out String current_sublevel, out String current_quize, out String total_quize)
        {
            String[] splitedQuize = text.Split('\n');

            String[] Level = splitedQuize[0].Replace(" Quize: ", "").Replace("🔸 Level: ", "").Split('_');
            String[] Sublevel = Level[1].Split(',');
            String[] Quize = Sublevel[1].Split('/');

            current_level = Level[0];
            current_sublevel = Sublevel[0];
            current_quize = Quize[0];
            total_quize = Quize[1];
        }

        /// <summary>
        /// Create level by image url
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="level"></param>
        /// <param name="sublevel"></param>
        /// <returns></returns>

        private String CreateLevelImageUrl(long userid, int level, int sublevel, int points)
        {
            return "<a href=\"" + "http://bots.narcissoft.com/magicspeak/bot/LevelImage?" + "level=" + level.ToString() + "&subLevel=" + sublevel.ToString() + "&userid=" + userid.ToString() + "&points=" + points.ToString() + "\">🔸</a> Level: " + level.ToString() + "_" + sublevel.ToString();
        }
        
        /// <summary>
        /// Create single voice level url
        /// </summary>
        /// <param name="level"></param>
        /// <param name="sublevel"></param>
        /// <param name="totalCount"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        private String CreateVoiceUrl(String level, String sublevel, String totalCount, String file)
        {
            return "<a href=\"" + "http://bots.narcissoft.com/" + level.ToString() + sublevel.ToString() + file + ".ogg" + "\">🔸</a> Level: " + level + "_" + sublevel + "\n🎵 Voice: " + totalCount + '/' + file;
        }

        /// <summary>
        /// Read first line of received Inline Keyboard Markup text and parse it for retrieved current level and sublevel.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="current_level"></param>
        /// <param name="current_sublevel"></param>
        private void ParseImageLableLevel(String text, out String str_current_level, out String str_current_sublevel)
        {
            String[] current_level_info = text.Replace("🔸 Level: ", "").Split('_');
            str_current_level = current_level_info[0];
            str_current_sublevel = current_level_info[1];
        }

        /// <summary>
        /// Parse level when text has tag.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="str_current_level"></param>
        /// <param name="str_current_sublevel"></param>
        private void ParseHashTagedLevel(String text, out String str_current_level, out String str_current_sublevel)
        {
            String[] splited_text = text.Split('\n');
            String[] splited_level = splited_text[0].Split(' ');
            int iLevel = Convert.ToInt16(splited_level[1].Replace("#Level_", ""));
            int iSublevel = Convert.ToInt16(splited_level[2].Replace("#Sublevel_", ""));
            str_current_level = IntToString(iLevel);
            str_current_sublevel = IntToString(iSublevel);
        }

        /// <summary>
        /// Parse text of single voice
        /// </summary>
        /// <param name="text"></param>
        /// <param name="str_current_level"></param>
        /// <param name="str_current_sublevel"></param>
        /// <param name="str_total_count"></param>
        /// <param name="str_current_line"></param>
        private void ParseVoiceOneLineLable(String text, out String str_current_level, out String str_current_sublevel, out String str_total_count, out String str_current_line)
        {
            String[] splited_text = text.Split('\n');
            String[] current_level_info = splited_text[0].Replace("🔸 Level: ", "").Split('_');
            String[] current_voice_info = splited_text[1].Split('/');
            str_total_count = current_voice_info[0].Replace("🎵 Voice: ", "");
            str_current_level = current_level_info[0];
            String[] splited_sublevel = current_level_info[1].Split('/');
            str_current_sublevel = splited_sublevel[0];
            str_current_line = current_voice_info[1];
        }

        private void HandleLevelMenuByPoint(TelegramTypes.X_Update xCallback, int current_level, int current_sublevel, int points)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            long chat_id = xCallback.callback_query.from.id;
            long message_id = xCallback.callback_query.message.message_id;
            if (points == -1)
            {
                inlineKeyboardMarkup = CreateLevelMenu(false);
                _telegram_interface.editMessageText(chat_id, message_id, CreateLevelImageUrl(chat_id, current_level, current_sublevel, points), inlineKeyboardMarkup, "HTML");
            }
            else
            {
                inlineKeyboardMarkup = CreateLevelMenu(true);
                _telegram_interface.editMessageText(chat_id, message_id, CreateLevelImageUrl(chat_id, current_level, current_sublevel, points), inlineKeyboardMarkup, "HTML");
            }
        }

        /// <summary>
        /// Convert integer value to string by adding 0 for number is less than 9.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private String IntToString(int number)
        {
            String retValue = "";
            if (number <= 9)
            {
                retValue = ("0" + number.ToString());
            }
            else
            {
                retValue = number.ToString();
            }
            return retValue;
        }
    }
}