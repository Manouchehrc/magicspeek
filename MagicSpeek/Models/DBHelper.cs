﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MagicSpeek.Models
{
    public class DBHelper
    {
        public static void getUserLevel(long user_id, long nInvitedBy, string firstname, string lastname, string username, out bool isNewUser, out int level, out int sublevel, out int points)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakGetUserLevel";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the output ISNEWUSER parameter
            param = comm.CreateParameter();
            param.ParameterName = "@IsNewUser";
            param.Direction = ParameterDirection.Output;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the INVEITEBY parameter
            param = comm.CreateParameter();
            param.ParameterName = "@InvitedByID";
            param.Value = nInvitedBy;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the FIRSTNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@FisrtName";
            param.Value = firstname;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // create the LASTNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LastName";
            param.Value = lastname;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // create the USERNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@UserName";
            param.Value = username;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            DataTable dtUserData = GenericDataAccess.ExecuteSelectCommand(comm);

            if (dtUserData.Rows.Count > 0)
            {
                level = Convert.ToInt16(dtUserData.Rows[0]["LevelNo"].ToString());
                sublevel = Convert.ToInt16(dtUserData.Rows[0]["SublevelNo"].ToString());
                points = Convert.ToInt16(dtUserData.Rows[0]["Points"].ToString());
                isNewUser = Convert.ToInt16(comm.Parameters["@IsNewUser"].Value) == 0 ? false : true;
            }
            else
            {
                level = 0;
                sublevel = 0;
                points = -1;
                isNewUser = false;
            }
        }


        public static int getUserLevelPoints(long user_id, int level, int sublevel)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakGetUserLevelPoints";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LevelNO";
            param.Value = level;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = sublevel;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            try
            {
                return Convert.ToInt16(GenericDataAccess.ExecuteScalar(comm).ToString());
            }
            catch(Exception ex)
            {
                return Convert.ToInt16("0");
            }
            
        }

        public static int UnlockLevel(long user_id, int level, int sublevel)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakUnlockLevel";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LevelNO";
            param.Value = level;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = sublevel;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            return GenericDataAccess.ExecuteNonQuery(comm);
        }

        public static int setUserLevelPoints(long user_id, int level, int sublevel, int points)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakGetUserLevelPoints";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LevelNO";
            param.Value = level;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = sublevel;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the POINTS parameter
            param = comm.CreateParameter();
            param.ParameterName = "@Points";
            param.Value = points;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            return GenericDataAccess.ExecuteNonQuery(comm);
        }

        public static List<string> getConversation(int current_level, int current_sublevel)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetConversation";

            // create the LEVELNO parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@LevelNo";
            param.Value = current_level;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = current_sublevel;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            DataTable dtConversation = GenericDataAccess.ExecuteSelectCommand(comm);

            List<string> conversion = new List<string>();
            for (int i = 0; i < dtConversation.Rows.Count; i++)
            {
                conversion.Add(dtConversation.Rows[i]["Context"].ToString());
            }

            return conversion;
        }

        public static string getConversionLine(int current_level, int current_sublevel, int LineNo)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetConversationLine";

            // create the LEVELNO parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@LevelNo";
            param.Value = current_level;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = current_sublevel;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the  LINE NO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LineNo";
            param.Value = LineNo;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            return GenericDataAccess.ExecuteScalar(comm);
        }

        public static string getConversionVoice(long chat_id, int current_level, int current_sublevel)
        {
            String voice_link = "http://bots.narcissoft.com/311.ogg";
            return voice_link;
        }

        public static List<string> GetQuizeInfo(int level, int sublevel, int quizeNo)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetQuizeInfo";

            // create the QuizeNo parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@QuizeNo";
            param.Value = quizeNo;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LevelNO";
            param.Value = level;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = sublevel;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            DataTable dtUserData = GenericDataAccess.ExecuteSelectCommand(comm);

            List<string> QuizeInfo = new List<string>();

            if (dtUserData.Rows.Count > 0)
            {
                QuizeInfo.Add(dtUserData.Rows[0]["QuizeNo"].ToString());
                QuizeInfo.Add(dtUserData.Rows[0]["Question"].ToString());

                string[] Answers = Regex.Split(dtUserData.Rows[0]["Answers"].ToString(), "\r\n");
                foreach (string answer in Answers)
                {
                    QuizeInfo.Add(answer);
                }
                QuizeInfo.Add(dtUserData.Rows[0]["Answer"].ToString());
            }
            else
            {
            }

            return QuizeInfo;
        }
    }
}